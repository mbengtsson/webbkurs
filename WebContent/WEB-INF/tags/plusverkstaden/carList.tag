<%@ attribute name="cars" required="true" rtexprvalue="true"
	type="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class=table id=list>
	<table class=list>

		<tr>
			<th>Reg no</th>
			<th>Year</th>
			<th>Brand</th>
			<th>Fuel consumption</th>
			<th>Owner</th>
			<th>Edit</th>
		</tr>

		<c:forEach var="car" items="${cars}">
			<tr>
				<td>${car.regNo }</td>
				<td>${car.yearModel }</td>
				<td>${car.brand.niceName }</td>
				<td>${car.fuelConsumption }</td>
				<td>${car.owner.fullName }</td>
				<td><a
					href='<c:url value="/plusverkstaden/showCar.servlet?regNo=${car.regNo }" />'
					class="button">Show</a></td>
			</tr>


		</c:forEach>

	</table>
</div>