<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<link rel="stylesheet" href="<%=request.getContextPath()%>/styles/global.css">

<style>
ul {
	list-style: none;
	margin: 0;
	padding: 0;
}

div#countries {
	border: 1px solid #999;
	background-color: #FFFF99;
	font-size: 150%;
	padding: 1em;
}
</style>

<body>

	<h1>JSTL</h1>

	<h2>Using EL vs c:out</h2>

	<p>Attributes from the server can be outputted using EL only, or by
		wrapping the output in c:out. Look in the source code. The code is
		different depending on which solution you use.</p>
	<table class="dataTable">
		<tr>
			<th>Text 1 using EL</th>
			<td>${text1 }</td>
		</tr>
		<tr>
			<th>Text 1 using c:out</th>
			<td><c:out value="${text1 }"></c:out></td>
		</tr>
		<tr>
			<th>Text 2 using EL</th>
			<td>${text2 }</td>
		</tr>
		<tr>
			<th>Text 2 using c:out (escapeXml=true)</th>
			<td><c:out value="${text2 }" escapeXml="true"></c:out></td>
		</tr>
		<tr>
			<th>Text 2 using c:out (escapeXml=false)</th>
			<td><c:out value="${text2 }" escapeXml="false"></c:out></td>
		</tr>

	</table>
	
		<h2>c:if</h2>

	<form action="<%=request.getContextPath()%>/jstl.servlet">

		<p>Do you want to display detailed information?</p>
		<input type="radio" name="showDetailedInformation" value="false">
		No, thanks <input type="radio" name="showDetailedInformation" value="true">
		Yes, please <input type="submit" value="Select">
	</form>
	
	<c:if test="${not empty detailedInformation}">
	
		INFO: ${detailedInformation }
			
	</c:if>
	
	<h2>c:choose</h2>

	<form action="<%=request.getContextPath()%>/jstl.servlet">

		<p>Select a car</p>
		<select name="selectedCar">
			<option value="">- no car selected -</option>
			<c:forEach items="${cars}" var="car">
				<option value="${car.regNo}">${car.regNo}-
					${car.brand.niceName}</option>
			</c:forEach>
		</select> <input type="submit" value="Select car">
	</form>
	
	<c:choose>
	<c:when test="${not empty selectedCar }">
		The selected car is: ${selectedCar.regNo } - ${selectedCar.brand.niceName}
	</c:when>
	<c:otherwise>
		No car is selected
	</c:otherwise>
	
	
	</c:choose>

	<h2>Using c:forEach</h2>


	<table class="dataTable">
		<tr>
			<th>Reg number</th>
			<th>Brand</th>
			<th>Fuel consumption</th>
			<th>Year model</th>
			<th>Owner</th>

		</tr>
		
		<c:forEach var="car" items="${cars }">
			<tr>
				<td>${car.regNo }</td>
				<td>${car.brand.niceName }</td>
				<td>${car.fuelConsumption}</td>
				<td>${car.yearModel }</td>
				<td>${car.owner.firstName } ${car.owner.lastName }</td>
			</tr>
			
		</c:forEach>

	</table>

	<table class="dataTable">
		<tr>
			<th>First name</th>
			<th>Last name</th>
			<th>Address</th>
			<th>Zip</th>
			<th>City</th>
			<th>Cars</th>
		</tr>
		
		<c:forEach var="owner" items="${owners }">
			<tr>
				<td>${owner.firstName }</td>
				<td>${owner.lastName }</td>
				<td>${owner.address }</td>
				<td>${owner.zipCode }</td>
				<td>${owner.city }</td>
				<td>
					<c:forEach var="car" items="${owner.cars }">
						${car.regNo } - ${car.brand.niceName } <br>
					
					</c:forEach>
				
				</td>
			</tr>
			
		</c:forEach>
	</table>
	




	<h2>Using c:forEach - with loop status</h2>

	<p>Iterate all countries. Use c:forEach varStatus to control the
		iteration. Output a comma between each country, but check when we
		reach the last iteration and don�t print a comma after the last
		country.</p>

	<div id="countries">
		
		<c:forEach var="country" items="${countries}" varStatus="status" >
			${country } <c:if test="${!status.last}">, </c:if>
		
		</c:forEach>
		
	</div>

	<p>
</body>

</html>