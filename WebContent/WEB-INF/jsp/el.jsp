<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="<%= request.getContextPath() %>/styles/global.css">
</head>


<body>

	<h1>EL - Expression language</h1>

	<h2>Printing the values of a car</h2>
	
	<table class="dataTable">
		<tr>
			<th colspan ="2">Car</th>
		</tr>
		<tr>
			<td>Reg Number</td>
			<td>${Car.regNo }</td>
		</tr>
		<tr>
			<td>Year model</td>
			<td>${Car.yearModel }</td>
		</tr>
		<tr>
			<td>Brand</td>
			<td>${Car.brand }</td>
		</tr>
		<tr>
			<td>Fule consumption</td>
			<td>${Car.fuelConsumption }</td>
		</tr>
		<tr>
			<th colspan ="2">Owner</th>
		</tr>
		<tr>
			<td>First name</td>
			<td>${Car.owner.firstName }</td>
		</tr>
		<tr>
			<td>Last name</td>
			<td>${Car.owner.lastName }</td>
		</tr>
		<tr>
			<td>Address</td>
			<td>${Car.owner.address }</td>
		</tr>
		<tr>
			<td>Zip code</td>
			<td>${Car.owner.zipCode }</td>
		</tr>
		<tr>
			<td>City</td>
			<td>${Car.owner.city}</td>
		</tr>
	
	
	</table>

	
	<h2>Printing parameters</h2>

	<p>Enter the name of your favorite pet. Below you should print the
		pet's name using the sent request parameter.</p>

	<form action="<%=request.getContextPath()%>/expressionLanguage.servlet">

		<input type="text" name="pet"> <input type="submit"
			value="Send pet name">

	</form>

	<p>Your favorite pet is: ${param.pet }</p>

	<h2>Printing variables in different scope</h2>

	<dl>
		<dt>Request</dt>
		<dd>${requestScope.message }</dd>
		<dt>Session</dt>
		<dd>${sessionScope.message }</dd>
		<dt>Application</dt>
		<dd>${applicationScope.message }</dd>

	</dl>

	<h2>Printing headers</h2>
	
	<dl>
		<dt>Host</dt>
		<dd>${header.host }</dd>
		
	</dl>
	
	<h2>Printing values from the request</h2>
	<dl>
		<dt>URI</dt>
		<dd>${pageContext.request.requestURI}</dd>
		<dt>Method</dt>
		<dd>${pageContext.request.method}</dd>
	</dl>

</body>

</html>