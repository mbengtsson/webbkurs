<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="myDate" uri="myFunctions" %>
<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags/plusverkstaden"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>List Cars</title>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/styles/Cars.css">
</head>
<body>
	<jsp:include page="header.jsp">
		<jsp:param value="List cars" name="title"/>
		<jsp:param value="${myDate:today()}" name="date"/>
	</jsp:include>
	
	<nav>
		<a href='<c:url value="/plusverkstaden/editCar.servlet" />' class="button">Add new car</a>
	</nav>
	
	<myTags:carList cars="${cars}"/>
	
	<jsp:include page="footer.jsp"></jsp:include>	
</body>
</html>