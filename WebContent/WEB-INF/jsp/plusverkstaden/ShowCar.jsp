<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="myDate" uri="myFunctions" %>
<%@ page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Show Cars</title>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/styles/Cars.css">
</head>
<body>
	<jsp:include page="header.jsp">
		<jsp:param value="Show car: ${car.regNo }" name="title"/>
		<jsp:param value="${myDate:today()}" name="date"/>
	</jsp:include>

	

	<div class=table id=car>
		<h2>Car</h2>
		<table class=info>
			<tr>
				<th>Reg number</th>
				<td>${car.regNo }</td>
			</tr>
			<tr>
				<th>Year model</th>
				<td>${car.yearModel }</td>
			</tr>
			<tr>
				<th>Brand</th>
				<td>${car.brand.niceName }</td>
			</tr>
			<tr>
				<th>Fuel consumption</th>
				<td>${car.fuelConsumption }</td>
			</tr>
	
		</table>
	</div>
	
	<div class=table id=owner>
		<h2>Owner</h2>
		<table class=info>
			<tr>
				<th>First name</th>
				<td>${car.owner.firstName }</td>
			</tr>
			<tr>
				<th>Last name</th>
				<td>${car.owner.lastName }</td>
			</tr>
			<tr>
				<th>Address</th>
				<td>${car.owner.address }</td>
			</tr>
			<tr>
				<th>Zip code</th>
				<td>${car.owner.zipCode}</td>
			</tr>
			<tr>
				<th>City</th>
				<td>${car.owner.city}</td>
			</tr>
			
		
		</table>
	
	</div>

	<nav>
		
		<a href='<c:url value="/plusverkstaden/editCar.servlet?regNo=${car.regNo }" />' class="button">Edit Car</a>
		<a href='<c:url value="/plusverkstaden/listCars.servlet" />' class="button">Cancel</a>
	</nav>
		
	<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>