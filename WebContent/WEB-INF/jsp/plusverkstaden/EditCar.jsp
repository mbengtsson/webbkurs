<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="myDate" uri="myFunctions"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Edit Cars</title>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/styles/Cars.css">
</head>
<body>

	<c:set var="title" value="Edit car" />
	<c:set var="submit" value="Update car" />

	<c:if test="${newCar}">
		<c:set var="title" value="Create new car" />
		<c:set var="submit" value="Create car" />
	</c:if>

	<jsp:include page="header.jsp">

		<jsp:param value="${title}" name="title" />
		<jsp:param value="${myDate:today()}" name="date" />

	</jsp:include>

	<c:if test="${invalidReg}">
		<p class="warning">Missing reg number</p>
	</c:if>
	<c:if test="${invalidYear}">
		<p class="warning">Invalid year, must be between 1970 and 2013</p>
	</c:if>
	<c:if test="${invalidBrand}">
		<p class="warning">Missing brand</p>
	</c:if>
	<c:if test="${invalidFuel}">
		<p class="warning">Invalid fuel consumption, must be between 0.1
			and 3.0</p>
	</c:if>
	<c:if test="${invalidOwner}">
		<p class="warning">Missing owner</p>
	</c:if>


	<div class="table">
		<form
			action='<c:url value="/plusverkstaden/editCar.servlet"></c:url>'
			method="POST">

			<table class="info">
				<tr>
					<th>Reg no</th>
					<td><c:if test="${newCar}">
							<input type="text" name="reg" value="${car.regNo}">
						</c:if> <c:if test="${not newCar}">
				${car.regNo}<input type="hidden" name="reg" value="${car.regNo}">
						</c:if></td>
				</tr>
				<tr>
					<th>Year</th>
					<td><input type="text" name="year" value="${car.yearModel}"></td>
				</tr>
				<tr>
					<th>Brand</th>
					<td><select name="brand">

							<c:if test="${newCar}">
								<option value="empty"></option>
							</c:if>

							<c:forEach var="brand" items="${brands}">
								<c:set var="selected" value=""></c:set>
								<c:if test="${car.brand eq brand}">
									<c:set var="selected" value="selected"></c:set>
								</c:if>

								<option value='<c:out value="${brand}" />' ${selected}><c:out
										value="${brand.niceName}" />
								</option>
							</c:forEach>
					</select></td>
				</tr>
				<tr>
					<th>Fuel consumption</th>
					<td><input type="text" name="fuel"
						value="${car.fuelConsumption}"></td>
				</tr>
				<tr>
					<th>Owner</th>
					<td><select name="owner">

							<c:if test="${newCar}">
								<option value="empty"></option>
							</c:if>

							<c:forEach var="owner" items="${owners}">
								<c:set var="selected" value=""></c:set>
								<c:if test="${car.owner eq owner}">
									<c:set var="selected" value="selected"></c:set>
								</c:if>

								<option value='<c:out value="${owner.socialSecurityNo}" />'
									${selected}><c:out value="${owner.fullName}" />
								</option>
							</c:forEach>
					</select></td>
				</tr>

			</table>


			<input id="submit" type="submit" value="${submit}"> <a
				href='<c:url value="/plusverkstaden/listCars.servlet" />' class="button">Cancel</a>

		</form>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>