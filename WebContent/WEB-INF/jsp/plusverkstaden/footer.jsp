<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<footer>
	<h4>Recently edited cars</h4>
	<ul>
		<c:forEach var="carWithName" items="${carsWithNames}">
		<li>
			<c:out value="${carWithName}"></c:out>
		</li>
		</c:forEach>	
	</ul>
</footer>