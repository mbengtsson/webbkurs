<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isErrorPage="true" %>
<%@ taglib prefix="myDate" uri="myFunctions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/styles/Cars.css">
<title>404</title>
</head>
<body>
	<jsp:include page="../header.jsp">
		<jsp:param value="Error" name="title"/>
		<jsp:param value="${myDate:today()}" name="date"/>
	</jsp:include>
	
	<div class="error">
		<img alt="404" src="/webb/images/404.jpg">
	
		<h2>Your page could not be found</h2>
		<p>How about going back to the <a href="<%=request.getContextPath()%>/plusverkstaden/listCars.servlet">start page</a>?</p>
	</div>
	
	<jsp:include page="../footer.jsp"></jsp:include>	

</body>
</html>