<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isErrorPage="true" %>
<%@ taglib prefix="myDate" uri="myFunctions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/styles/Cars.css">
<title>exception</title>
</head>
<body>
	<jsp:include page="../header.jsp">
		<jsp:param value="Error" name="title"/>
		<jsp:param value="${myDate:today()}" name="date"/>
	</jsp:include>
	
	<div class="error">
		<img alt="error" src="<%=request.getContextPath()%>/images/error.png">
		
		<h2>Oops, something bad happened</h2>
		<h4>An exception was thrown:</h4>
		<p>${pageContext.exception}</p>
	</div>
	<jsp:include page="../footer.jsp"></jsp:include>
</body>
</html>