<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<title>Game statistics</title>

<link rel="stylesheet"
	href="<c:url value="/networkgame/networkgame.css" />">
</head>
<body>

	<div class="outerFrame">

		<h1>Game server - statistics</h1>

		<table class="dataTable">
			<tr>
				<th>No of players</th>
				<td>${noOfPlayers}</td>
			</tr>
			<tr>
				<th>Pot</th>
				<td>${pot}</td>
			</tr>
			<tr>
				<th>Total credits among all players</th>
				<td>${totalAmount}</td>
			</tr>
		</table>
	</div>
</body>
</html>