<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Play</title>
<link rel="stylesheet"
	href="<c:url value="/networkgame/networkgame.css" />">

<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {

		startWebSockets();

		$("#playForm").submit(function(event) {

			event.preventDefault();
			
			var posting = $.post($(this).attr("action"), $("#playForm").serialize());
			
 			posting.done(function(jsonObject) {
 								
				console.log(jsonObject);
				
				var win = jsonObject.win;
				
				if (win == 0) {
					$("#msgText").html("Sorry, you lost.");
					$("#msgText").removeClass("win");
					$("#msgText").addClass("lost");
				} else {
					$("#msgText").html("You won " + win + " SEK");
					$("#msgText").removeClass("lost");
					$("#msgText").addClass("win");
				}
				
				$("#credit").html(jsonObject.credit);
			});
			

			
		});

	});

	function startWebSockets() {
		if ('WebSocket' in window){
			var socket = new WebSocket("ws://<%=request.getHeader("host")%><%=request.getContextPath()%>/networkgame/client/websocket.servlet");
			socket.onmessage = function(evt){
				
				var jsonObject = JSON.parse(evt.data);

				console.log(jsonObject);   
				var pot = jsonObject.pot;
				console.log(pot);
				$("#potsize").text(pot);
				   
				};
			
		} else {
			  alert("Sorry, WebSockets are not suported by your browser.")
			}
		
	}
</script>

</head>
<body class="play">

	<div class="outerFrame">

		<header>
			<div class="headerTitle">
				<h1>PlusCasino</h1>
			</div>
			<div class="headerCredit">
				<p>Your credit: <span id="credit">${sessionScope.gameSession.credit}</span> SEK</p>
				<p>
					Pot size: <span id="potsize">?</span>
				</p>
				<p>
					<a href="?logout=true">Log out</a>
				</p>
			</div>

		</header>


		<div id="msgDiv">
			<p id="msgText">Come on, try your luck!</p>
		</div>


		<form id="playForm"
			action="<%=request.getContextPath()%>/networkgame/client/play.servlet"
			method="POST">
			<p>

				Amount:<br> <input type="text" name="amount" id="amountField"
					size="6" value="${gameSession.amountLastBet }" /> <input
					type="submit" value="Play" id="playButton">
			</p>


		</form>

	</div>

</body>
</html>