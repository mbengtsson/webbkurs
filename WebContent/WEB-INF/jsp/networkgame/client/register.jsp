<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Register</title>

<link rel="stylesheet"
	href="<c:url value="/networkgame/networkgame.css" />">

<script type="text/javascript">
	function checkForm() {

		var nameInput = document.getElementById("nameInput");
		var amount = document.getElementById("amount");
		
		var error = "";
		
		if (nameInput.value.length == 0) {
			error += "You need to enter a name\n";
		}
		
		if (amount.value.length == 0) {
			error += "You need to enter an amount\n";
		} else if (isNaN(amount.value)) {
			error += "Amount need to be a number\n";
		}
		
		if (error.length == 0) {
			return true;
		}
		
		alert(error);
		
		return false;
	}
</script>


</head>
<body class="register">

	<div class="outerFrame">

		<h1>Welcome to PlusCasino</h1>

		<form
			action="<%=request.getContextPath()%>/networkgame/client/register.servlet"
			method="POST">

			<p>
				Name:<br> <input type="text" name="name" value="" size="20"
					id="nameInput">
			</p>
			<p>
				Amount to start with:<br> <input type="text" name="amount"
					value="" id="amount" size="6" maxlength="6">
			</p>

			<p>
				<input type="submit" value="Enter" onclick="return checkForm()">

			</p>

		</form>

	</div>


</body>
</html>