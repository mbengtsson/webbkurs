
<!DOCTYPE html>
<html>

<head>
<link rel="stylesheet" href="/webbkurs/styles/tagFiles.css">

</head>

<body>

	<h1>Tag files</h1>

	<p>Tag files is a way of reusing jsp snippets. It is a powerful
		tool to avoid duplicated code.</p>

	<h2>List all cars</h2>

	<p>Retrieve a list of all cars from the server (via a Servlet).
		Iterate each car and use a tag file to print the details of each car.
	</p>



	<h2>Print an info message</h2>

	<p>Use a tag file to print an info message. Use the body-content
		tag directive to specify the info message within the body of tag.</p>


	<p>This long text should be sent to the info message tag.</p>
	<p>Since it is so long we don�t want to add it using a normal
		attribute, so we put it in the body instead.</p>



</body>

</html>