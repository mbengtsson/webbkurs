<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>

<link rel="stylesheet" href="<c:url value="/styles/global.css" />">

</head>

<body>

	<h1>Set values to session and cookie</h1>

	<form action='<c:url value="/setSessionValues.servlet" />'
		method="POST">

		<p>
			Your name: <br> 
			<input type="text" name="name" />
		</p>

		<p>
			Your preferred language:<br> 
			
			<select name="language">
			
			<c:forEach var="language" items="${languages}">
			
				<option value='<c:out value="${language }" />'><c:out value="${language }" /></option>
			</c:forEach>
			
			</select>
		</p>
		
		<p><input type="submit" value="Submit" />
	</form>
	
	<p>If you have already submitted some values, you can go directly to the <a href="<c:url value="/servletexercises/jsp/sessions1/readSessionValues.jsp" />">overview page</a> to check if they are there. </p>

</body>

</html>