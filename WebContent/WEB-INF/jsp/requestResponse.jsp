<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>

<html>

<head>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/styles/global.css">
</head>

<body>

	<h1>Request response</h1>

	<p>This exercise shows the difference between GET and POST
		requests. It also shows the values that are sent as parameters,
		attributes and headers.</p>

	<ul class="noBullets">

		<li>Request method: <b><%=  request.getMethod() %></b></li>
		<li>Response status code: <b><%=  response.getStatus() %></b></li>
		<li>Response content type: <b><%=  response.getContentType() %></b></li>
	</ul>

	<p>
		<b>This is a list of all parameters:</b>
	</p>
	<table class="dataTable">
		<tr>
			<th>Parameter name</th>
			<th>Parameter value</th>
		</tr>
		
		<% 
			Enumeration<String> paramNames = request.getParameterNames();
	
			while(paramNames.hasMoreElements()) {
				
				String param = paramNames.nextElement();
				out.println("<tr><td>" + param + "</td><td>" + request.getParameterMap().get(param)[0] + "</td></tr>");
			}		
		%>
	
	</table>

	<p>
		<b>This is a list of all attributes:</b>
	</p>
	<table class="dataTable">
		<tr>
			<th>Attribute name</th>
			<th>Attribute value</th>
			
			<% 
				Enumeration<String> attribNames = request.getAttributeNames();
				
				while(attribNames.hasMoreElements()) {

					String attrib = attribNames.nextElement();
					out.println("<tr><td>" + attrib + "</td><td>" + request.getAttribute(attrib) + "</td></tr>");
				}
			%>
			
		</tr>
		
	</table>

	<p>
		<b>This is a list of all request headers:</b>
	</p>
	<table class="dataTable">
		<tr>
			<th>Header name</th>
			<th>Header value</th>
			
			<% 
				Enumeration<String> reqHeaderNames = request.getHeaderNames();
				
				while(reqHeaderNames.hasMoreElements()) {

					String reqHeader = reqHeaderNames.nextElement();
					out.println("<tr><td>" + reqHeader + "</td><td>" + request.getHeader(reqHeader) + "</td></tr>");
				}
			%>
		</tr>
		
	</table>

	<p>
		<b>This is a list of all response headers:</b>
	</p>
	<table class="dataTable">
		<tr>
			<th>Header name</th>
			<th>Header value</th>
			
			<% 
						
				Collection<String> respHeaderCollection = response.getHeaderNames();
				Iterator<String> respHeaderIterator = respHeaderCollection.iterator();
			
				while(respHeaderIterator.hasNext()) {
					
					String respHeader = respHeaderIterator.next();
					out.println("<tr><td>" + respHeader + "</td><td>" + response.getHeader(respHeader) + "</td></tr>");
				
				}
			%>
		</tr>
		
	</table>




	<h2>GET requests</h2>

	<ul class="noBullets">

		<li><a
			href="<%=request.getContextPath()%>/requestResponse.servlet?id=1">A
				link with an ID parameter = 1</a></li>
		<li><a
			href="<%=request.getContextPath()%>/requestResponse.servlet?id=2">A
				link with an ID parameter = 2</a></li>
		<li><a
			href="<%=request.getContextPath()%>/requestResponse.servlet?name=Kalle">A
				link with a name parameter</a></li>
		<li><a
			href="<%=request.getContextPath()%>/requestResponse.servlet?x=234&y=234&length=900&speed=145">A
				link with many parameters</a></li>

	</ul>

	<h2>POST requests</h2>
	<form action="<%=request.getContextPath()%>/requestResponse.servlet"
		method="POST">

		<table class="dataTable">
			<tr>
				<th>Name</th>
				<td><input type="text" name="name"></td>
			</tr>
			<tr>
				<th>Street</th>
				<td><input type="text" name="street"></td>
			</tr>
			<tr>
				<th>Phone</th>
				<td><input type="text" name="age" size="20"></td>
			</tr>


		</table>
		<input type="submit" value="Submit using POST">
	</form>
</body>

</html>