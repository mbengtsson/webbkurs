<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>The forum</title>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/styles/forum.css">
</head>
<body>

	<div class="wrapper">

		<c:choose>
			<c:when test="${not empty user}">
				<c:set var="name" value="${user.fullName}" />
			</c:when>
			<c:otherwise>
				<c:set var="name" value="guest"></c:set>
			</c:otherwise>
		</c:choose>

		<jsp:include page="header.jsp">
			<jsp:param value="Hello ${name}!" name="text" />
		</jsp:include>

		<div id="content">
			<table id="listThreads">

				<tr>
					<th class="topic">Topic</th>
					<th class="posts">Posts</th>
					<th class="author">Created by</th>
					<th class="edited">Last post</th>
				</tr>
				<c:forEach var="thread" items="${threads}">
					<tr>
						<td><a
							href='<c:url value="/forum/readThread.servlet?thread=${thread.ID}" />'>${thread.title}</a></td>
						<td>${thread.postCount}</td>
						<td>${thread.user.alias}</td>
						<td>${thread.edited}</td>
					</tr>
				</c:forEach>
			</table>
			<c:if test="${not empty user}">
				<a
					href="<c:url value="/forum/makePost.servlet?threadId=-1"></c:url>">New
					topic</a>
			</c:if>
		</div>
		<jsp:include page="sidebar.jsp"></jsp:include>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
</body>
</html>