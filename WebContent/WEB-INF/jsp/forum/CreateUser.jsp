<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>The forum - create account</title>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/styles/forum.css">
</head>
<body>

	<div class="wrapper">

		<jsp:include page="header.jsp">
			<jsp:param value="Create user" name="text" />
		</jsp:include>

		<div id="content">
			<div class="warning">${warning}</div>

			<form id="userform"
				action='<c:url value="/forum/createUser.servlet"></c:url>'
				method="POST">
				<table>
					<tr>
						<td>Username:</td>
						<td><input name="username"></td>
					</tr>
					<tr>
						<td>First name:</td>
						<td><input name="firstname"></td>
					</tr>
					<tr>
						<td>Last name:</td>
						<td><input name="lastname"></td>
					</tr>
					<tr>
						<td>Password:</td>
						<td><input type="password" name="password"></td>
					</tr>
				</table>
				<input class="button" type="submit" value="Create account">
				<input class="button" type="button"
					onclick="window.location.replace('<%=request.getContextPath()%>/forum/logIn.servlet')"
					value="Cancel" />
			</form>

		</div>
		<jsp:include page="sidebar.jsp"></jsp:include>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
</body>
</html>