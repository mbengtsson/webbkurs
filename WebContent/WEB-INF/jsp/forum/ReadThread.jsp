<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>The forum - ${thread.title}</title>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/styles/forum.css">
<script type="text/javascript">

function confirmDelete(index) {
	if (confirm("Are you sure you want to remove the post?")) {
		document.getElementById("delPost" + index).submit();
	}
}
</script>

</head>
<body>

	<div class="wrapper">

		<c:choose>
			<c:when test="${not empty user}">
				<c:set var="name" value="${user.fullName}" />
			</c:when>
			<c:otherwise>
				<c:set var="name" value="guest"></c:set>
			</c:otherwise>
		</c:choose>

		<jsp:include page="header.jsp">
			<jsp:param value="Hello ${name}!" name="text" />
		</jsp:include>

		<div id="content">
			<table id="readThread">
				<tr>
					<th class="author">Author</th>
					<th>Topic: <c:out value="${thread.title}" /></th>
				</tr>
				<c:forEach var="post" items="${posts}" varStatus="counter">
					<tr>
						<td>${post.user.alias}</td>
						<td><h4>${post.title }</h4>
							<div class="date">${post.date}</div>
							<hr>
							<p>${post.htmlMessage }</p> <c:if test="${user == post.user}">
								<a href='<c:url value="/forum/makePost.servlet?threadId=${thread.ID}&postId=${counter.index}"></c:url>'>Edit</a> |
								
								<form class="delForm" id="delPost${counter.index}"
									action="<%=request.getContextPath()%>/forum/readThread.servlet"	method="post">
									<input type="hidden" name="deleteId" value="${counter.index}" />
									<input type="hidden" name="thread" value="${thread.ID}" /> 
									<a href="javascript:;" onclick="confirmDelete(${counter.index});">Delete</a>
								</form>
							</c:if></td>
					</tr>


				</c:forEach>

			</table>
			<c:if test="${not empty user}">
				<a
					href="<c:url value="/forum/makePost.servlet?threadId=${thread.ID}"></c:url>">Make
					reply</a> | 
			</c:if>
			<a href="<c:url value="/forum/listThreads.servlet"></c:url>">Go
				back</a>
		</div>
		<jsp:include page="sidebar.jsp"></jsp:include>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>




</body>
</html>