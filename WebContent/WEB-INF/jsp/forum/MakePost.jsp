<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>The forum - post</title>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/styles/forum.css">
</head>
<body>

	<div class="wrapper">

		<jsp:include page="header.jsp">
			<jsp:param value="Hello ${user.fullName}!" name="text" />
		</jsp:include>
		
		<c:choose>
			<c:when test="${newThread}">
				<c:set var="message" value="Make new topic" />
				<c:set var="title" value="" />
				<c:set var="id" value="-1" />
				<c:url var="last" value="/forum/listThreads.servlet" />
			</c:when>
			<c:otherwise>
				<c:set var="message" value="Make reply to &quot;${thread.title}&quot;" />
				<c:set var="title" value="RE: ${thread.title}" />
				<c:set var="id" value="${thread.ID}" />
				<c:url var="last" value="/forum/readThread.servlet?thread=${id}" />
			</c:otherwise>			
		</c:choose>
		
		<c:if test="${not empty post}">
			<c:set var="message" value="Edit post &quot;${post.title}&quot;" />
			<c:set var="title" value="${post.title}" />
		</c:if>

		<div id="content">
			<div class="warning">				
				<c:out value="${warning}"></c:out>	
			</div>
			<form action='<c:url value="/forum/makePost.servlet"></c:url>'
				method="POST">
				<table id="makePost">
					<tr>
						<th colspan="2">${message}</th>
					</tr>
					<tr>
						<td class="title">Title:</td>
						<td><input id="newTitle" name="title" value="${title}" /></td>
					</tr>
					<tr>
						<td class="title">Message:</td>
						<td><textarea name="message">${post.message}</textarea>
							<input type="hidden" name="threadId" value="${id}" />
							<input type="hidden" name="postId" value="${param.postId}" />
							<br>
							<input class="button" type="submit" value="Submit post"> 
							<input class="button" type="button" value="Cancel"	onClick="window.location.replace('${last}')"></td>
					</tr>
				</table>
			</form>
		</div>
		<jsp:include page="sidebar.jsp"></jsp:include>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>

</body>
</html>