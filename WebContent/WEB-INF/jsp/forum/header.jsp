<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<header>
	<h1>
		<a href="<c:url value="/forum/listThreads.servlet"></c:url>">The
			forum</a>
	</h1>

	<p>${param.text}

		<a id="login" href="<%=request.getContextPath()%>/forum/logIn.servlet"><img
			alt="*" src="<%=request.getContextPath()%>/images/forum/padlock.png">
			<c:choose>
				<c:when test="${not empty user}">
					Log out
				</c:when>
				<c:otherwise>
					Log in
				</c:otherwise>
			</c:choose> </a>
	</p>

</header>