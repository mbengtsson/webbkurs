<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
div#board {
	
	width: ${gameWidth}px;
	height: ${gameHeight}px;
	background-image: url(<%=request.getContextPath()%>/servletexercises/game/bg.jpg);
	background-repeat: no-repeat;
	border: 2px solid black;
}

img#hero {
	position: relative;
	top: ${yPos}px;
	left: ${xPos}px;
}

div#controls {
	position: relative;
}

a#up {
	position: absolute;
	top: 0px;
	left: 48px;
}
a#down {
	position: absolute;
	top: 96px;
	left: 48px;
}
a#left {
	position: absolute;
	top: 48px;
	left: 0px;
}
a#right {
	position: absolute;
	top: 48px;
	left: 96px;
}

table#slider {
	border-collapse: collapse;
	position: relative;
	top: 48px;
	left: 170px;
}

div#heroes {
	position: absolute;
	top: 24px;
	left: 350px;
}

</style>

<script type="text/javascript">
	
function setSpeed() {
	var url;
	
	url = "game.servlet?speed=";	
	url += document.getElementById("speed").value;
	
	window.location=url;
}

</script>

<title>Game</title>
</head>
<body>
	<div id=board>
		<img src="<%=request.getContextPath()%>/servletexercises/game/<c:out value="${hero}" />" width="${heroWidth}" height="${heroHeight}" id=hero>
	</div>
	
	<div id=controls>
	
		<a href= "game.servlet?direction=up" id="up"><img src="<%=request.getContextPath()%>/servletexercises/game/top.png"></a>
		<a href= "game.servlet?direction=left" id="left"><img src="<%=request.getContextPath()%>/servletexercises/game/left.png"></a>
		<a href= "game.servlet?direction=right" id="right"><img src="<%=request.getContextPath()%>/servletexercises/game/right.png"></a>
		<a href= "game.servlet?direction=down" id="down"><img src="<%=request.getContextPath()%>/servletexercises/game/down.png"></a>

		<table id=slider>
			<tr><td>Speed:</td></tr>
			<tr><td><input type="range" name="speed" value="${speed}" min="1" max="10" step="1" id="speed" onmouseup="setSpeed()"></td></tr>
		</table>

		<div id=heroes>
			<a href= "game.servlet?hero=penguin"><img src="<%=request.getContextPath()%>/servletexercises/game/penguin.png"></a>
			<a href= "game.servlet?hero=troll"><img src="<%=request.getContextPath()%>/servletexercises/game/troll.png"></a>
			<a href= "game.servlet?hero=hero" class="hero"><img src="<%=request.getContextPath()%>/servletexercises/game/hero.png"></a>
		</div>
	</div>
	
</body>
</html>