<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<link rel="stylesheet"
	href="<%=request.getContextPath()%>/styles/global.css">

<body>

	<jsp:include page="/servletexercises/jsp/sessions2/header.jsp" />

	<h1>Show car</h1>

	<table class="dataTable">
		<tr>
			<th>Reg number</th>
			<td>[Reg no]</td>
		</tr>
		<tr>
			<th>Year model</th>
			<td>[Year model]</td>
		</tr>
		<tr>
			<th>Brand</th>
			<td>[Brand]</td>
		</tr>
		<tr>
			<th>Fuel consumption</th>
			<td>[Fuel consumption]</td>
		</tr>
		<tr>
			<th>Owner</th>
			<td>[Owner]</td>
		</tr>

	</table>

	<form action='' method="POST">

		<input type="hidden" name="clearSelectedCar" value="true"> <input
			type="submit" value="Select another car">

	</form>



</body>

</html>