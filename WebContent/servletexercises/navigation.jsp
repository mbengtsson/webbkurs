<!DOCTYPE html>
<html>
<head>
<title>Navigation</title>
<link rel="stylesheet" href="doNotChange.css">

<style>

body {
	font-family: Verdana;
	
}

h1 {
	font-size: 1.2em;
	margin: .4em 0;
}


</style>

</head>
<body>

	<a href="start.htm" target="content">Start</a>


	<h1>Servlets</h1>
	<ul>
		<li><a href="<%= request.getContextPath() %>/requestResponse.servlet" target="content">Request & response</a></li>
		<li><a href="<%= request.getContextPath() %>/plusverkstaden/listCars.servlet" target="content">Plusverkstaden - Setup</a></li>
		<li><a href="<%= request.getContextPath() %>/game.servlet" target="content">Basic game (VG)</a></li>
	</ul>
	
	<h1>JSP</h1>
	<ul>
		<li><a href="<%= request.getContextPath() %>/expressionLanguage.servlet" target="content">EL - Expression language</a></li>
		<li><a href="<%= request.getContextPath() %>/jstl.servlet" target="content">JSTL</a></li>
		<li>Plusverkstaden - EL & JSTL</li>
	</ul>
	
	<h1>Taggar & includes</h1>
	<ul>
		<li>Plusverkstaden - Header & footer</li>
		<li>Plusverkstaden - EL function (VG)</li>
		<li>Plusverkstaden - Tag file</li>
		<li><a href="<%= request.getContextPath() %>/servletexercises/jsp/customTags.jsp" target="content">Custom tags (VG)</a></li>
	</ul>
	
	<h1>Forms</h1>
	<ul>
		<li>Plusverkstaden - Forms</li> 
		<li>Plusverkstaden - Error handling</li>
	</ul>
	
	<h1>Filter och sessions</h1>
	<ul>
		<li><a href="<%= request.getContextPath() %>/setSessionValues.servlet" target="content">Sessions - Basic</a></li>
		<li>Plusverkstaden - Filter & session (VG)</li>
	
	</ul>
	
		<h1>Hemuppgift</h1>
	<ul>
		<li><a href="<%= request.getContextPath() %>/forum/listThreads.servlet" target="content">Forum</a></li>
			
	</ul>
	
	
</body>
</html>