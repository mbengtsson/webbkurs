<!DOCTYPE html>
<html>


<head>

<link rel="stylesheet" href="<%= request.getContextPath() %>/styles/global.css">

</head>
<body>

	<h1>Old fashioned JSP</h1>

	<p>This page is designed to look into some old fashioned jsp techniques. They are really not best practises, 
	but they give you an idea how jsp behaves behind the scenes. </p>
	
	<p>In this exercise we submit a name, and we count how many names have been entered since the server was started. 
	
	<form action="<%= request.getContextPath() %>/servletexercises/jsp/jsp_basics.jsp">

		<input type="text" name="name"> <input type="submit"
			value="Submit name">
	</form>

	
</body>

</html>