<%@ taglib prefix="myTags" uri="myFunctions" %>

<html>

<head>


</head>

<body>

	<h1>Custom tags</h1>
	
	<myTags:getCar regNo="ABC-123" carAttributeName="car">
	
	<p>The owner of ${car.regNo} is ${car.owner.firstName} ${car.owner.lastName} who lives in ${car.owner.city}.</p>
	<p>The car ${car.regNo} from ${car.brand.niceName} has a fuel consumption of ${car.fuelConsumption} l/10km</p>
	</myTags:getCar>

	
</body>

</html>