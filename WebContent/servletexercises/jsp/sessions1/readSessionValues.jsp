<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>

<link rel="stylesheet"	href="<c:url value="/styles/global.css" />">

</head>

<body>

	<h1>Read values from session and cookie</h1>

	<p>Hello <b>${sessionScope.name}</b>! (This value is taken from the session)</p>
	
	<p>Your preferred language is <b>${cookie.language.value}</b>. (This value is taken from a cookie)</p>
	
	<p><p><a href="<c:url value="/setSessionValues.servlet" />">Back to the form</a>. </p>
</body>

</html>