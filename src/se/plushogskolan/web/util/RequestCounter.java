package se.plushogskolan.web.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

/**
 * A class for counting web requests.
 * 
 * This class is implemented as a Singleton. That means there is only one
 * instance of this class. This is an old fashioned technique for having access
 * to the same java instance across the whole codebase. Later on we will use
 * more modern techniques for achieving the same effect.
 * 
 * The singleton pattern has a private constructor so it can not be instantiated
 * from the outside. Instead we use the static getInstance() method to get an
 * instance, and this instance will always be the same.
 * 
 * This class counts how many times an URL has been called, and which type of
 * http method that was used (normally GET or POST).
 * 
 */
public class RequestCounter {
	Logger log = Logger.getLogger(RequestCounter.class);
	private static RequestCounter counter;

	/*
	 * These fields should be updated on each request.
	 */
	private Map<String, Integer> uriCounts = new HashMap<String, Integer>();
	private Map<String, Integer> methodCounts = new HashMap<String, Integer>();

	private RequestCounter() {

	}

	public static RequestCounter getInstance() {

		if (counter == null) {
			counter = new RequestCounter();
		}
		return counter;
	}

	public void handleRequest(HttpServletRequest req) {

		// Implement this method.
	}

	public Map<String, Integer> getUriCounts() {
		return this.uriCounts;
	}

	public Map<String, Integer> getMethodCounts() {
		return this.methodCounts;
	}

	/**
	 * A helper method for updating the count associated with a given key. If
	 * the key is not yet stored this method will store it in the Map and set
	 * count = 1. If the key already exist the count is updated by 1.
	 */
	private void addNewCount(String key, Map<String, Integer> map) {

		// Implement this method (if you want to)

		// Since we are using two maps with similar behavior, it is a good thing
		// to reuse code that is identical.
	}
}
