package se.plushogskolan.web.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import se.plushogskolan.web.domain.cars.Car;
import se.plushogskolan.web.domain.cars.CarService;

public class GetCarTag extends SimpleTagSupport{

	private String regNo;
	private String carAttributeName;
	

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public void setCarAttributeName(String carAttributeName) {
		this.carAttributeName = carAttributeName;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		
		Car car = CarService.getInstance().getCarByRegNo(regNo);
		
		getJspContext().setAttribute(carAttributeName, car);
		getJspBody().invoke(null);
		
	}

}
