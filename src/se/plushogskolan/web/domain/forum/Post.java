package se.plushogskolan.web.domain.forum;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Post {

	private User user;
	private String title;
	private String message;
	private String date;

	public Post(User user, String title, String message) {

		setUser(user);
		setTitle(title);
		setMessage(message);

		SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		date = simpleDate.format(new Date());
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getHtmlMessage() {
		return formatHtml(this.message);
	}

	public String getMessage() {
		return message;
	}

	public String getDate() {
		return date;
	}

	private String formatHtml(String text) {

		text = text.replaceAll("<", "&lt;");
		text = text.replaceAll(">", "&gt;");

		text = text.replaceAll("\r\n", "<br>");
		text = text.replaceAll("\r", "<br>");
		text = text.replaceAll("\n", "<br>");
		text = text.replaceAll("\t", "&nbsp;&nbsp;&nbsp;");
		text = text.replaceAll("  ", " &nbsp;");

		text = text.replaceAll("\\[b\\]", "<b>");
		text = text.replaceAll("\\[/b\\]", "</b>");
		text = text.replaceAll("\\[i\\]", "<i>");
		text = text.replaceAll("\\[/i\\]", "</i>");
		text = text.replaceAll("\\[u\\]", "<u>");
		text = text.replaceAll("\\[/u\\]", "</u>");

		text = text.replaceAll("\\[edited\\]", "<div id=\"edited\">edited: ");
		text = text.replaceAll("\\[/edited\\]", "</div>");

		return text;
	}
}
