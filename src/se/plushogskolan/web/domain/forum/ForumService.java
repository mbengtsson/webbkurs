package se.plushogskolan.web.domain.forum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ForumService {

	private static ForumService instance;

	private Map<String, User> users;
	private List<ForumThread> threads;

	private ForumService() {

		users = new HashMap<String, User>();
		threads = new ArrayList<ForumThread>();

		User thar = new User("Thar", "123", "Marcus", "Bengtsson");
		createUser(thar);

		ForumThread firstTopic = new ForumThread(thar, "First Topic");
		Post firstPost =
				new Post(
						thar,
						"First Topic",
						"Welcome to the forum"
								+ "\r\n\r\nPlease create an account and login to explore it and make some posts."
								+ "\r\nYou can use these tags &#91;b&#93;[b]bold[/b]&#91;/b&#93;, &#91;i&#93;[i]italic[/i]&#91;/i&#93; "
								+ "and &#91;u&#93;[u]underline[/u]&#91;/u&#93; to format your posts, html tags are disabled for safety. "
								+ "Each user can edit and delete their own posts but only read others."
								+ "\r\n\r\nYou can find the source-code for: "
								+ "\r\n\t* the domain-model under: /src/se/plushogskolan/web/domain/forum/"
								+ "\r\n\t* the servlets under: /src/se/plushogskolan/web/servlets/forum/"
								+ "\r\n\t* the jsp-pages under: /WebContent/WEB-INF/jsp/forum/"
								+ "\r\n\t* the style-sheet under: /WebContent/styles/forum/ and the images under WebContent/images/forum/"
								+ "\r\n\r\n There are responsive layouts for phones (-480px), tablets (481px-720px) and computers (721px-)."
								+ "\r\n\r\n /Marcus Bengtsson, GBJU13");
		firstTopic.addPost(firstPost);
		addThread(firstTopic);

	}

	public static ForumService getInstance() {
		if (instance == null) {
			instance = new ForumService();
		}

		return instance;
	}

	public void createUser(User user) {
		users.put(user.getAlias(), user);
	}

	public User getUserByAlias(String alias) {
		return users.get(alias);
	}

	public void addThread(ForumThread thread) {
		threads.add(0, thread);
	}

	public ForumThread getThread(int index) {
		return threads.get(index);
	}

	public ForumThread getThreadByID(int id) {

		for (ForumThread thread : threads) {
			if (thread.getID() == id) {
				return thread;
			}
		}

		return null;
	}

	public void removeThread(ForumThread thread) {
		threads.remove(thread);
	}

	public List<User> getUsers() {
		return new ArrayList<User>(users.values());

	}

	public List<ForumThread> getThreads() {
		return threads;
	}

}
