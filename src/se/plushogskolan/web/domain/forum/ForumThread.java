package se.plushogskolan.web.domain.forum;

import java.util.ArrayList;
import java.util.List;

public class ForumThread {

	private static int nextID = 0;
	private final int ID;

	private User user;
	private String title;
	private List<Post> posts;
	private String edited;
	private int postCount = 0;

	public ForumThread(User user, String title) {

		ID = nextID;
		nextID++;

		posts = new ArrayList<Post>();

		setUser(user);
		setTitle(title);

	}

	public int getID() {
		return ID;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setEdited(String edited) {
		this.edited = edited;
	}

	public String getEdited() {
		return edited;
	}

	public List<Post> getPosts() {
		return posts;
	}

	public void addPost(Post post) {
		posts.add(post);
		setEdited(post.getDate());
		postCount++;
	}

	public void removePost(int index) {
		posts.remove(index);
		postCount--;
	}

	public int getPostCount() {
		return postCount;
	}

}
