package se.plushogskolan.web.domain.forum;

public class User {

	private String alias;
	private String password;
	private String firstName;
	private String lastName;

	public User() {

	}

	public User(String alias, String password, String firstName, String lastName) {

		setAlias(alias);
		setPassword(password);
		setFirstName(firstName);
		setLastName(lastName);
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getAlias() {
		return alias;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getFullName() {
		return String.format("%s %s", firstName, lastName);
	}

}
