package se.plushogskolan.web.domain.cars;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A singleton CarService. Singleton means there can be only one instance, and
 * that instance is shared by all java classes. A CarService is retrieved by
 * calling the static getInstance() method.
 * 
 */
public class CarService {

	private static CarService service;

	private Map<String, Owner> owners = new HashMap<String, Owner>();
	private Map<String, Car> cars = new HashMap<String, Car>();

	public static CarService getInstance() {
		if (service == null) {
			service = new CarService();
		}
		return service;
	}

	/**
	 * A private constructor. This class can not be created from the outside.
	 * You need to use the getInstance() method to get an instance of
	 * CarService.
	 */
	private CarService() {

		Owner olle = new Owner("19800423-6545", "Olle", "Persson", "Storgatan 1", "416 72", "Göteborg");
		Owner kalle = new Owner("19920901-9812", "Kalle", "Johansson", "Torggatan 14", "412 23", "Göteborg");

		Car car1 = new Car("ABC-123", 2005, Brand.MERCEDES, 0.45, olle);
		Car car2 = new Car("DEF-456", 2011, Brand.VW, 0.56, kalle);
		Car car3 = new Car("GHI-789", 2013, Brand.VOLVO, 0.51, kalle);

		olle.getCars().add(car1);
		kalle.getCars().add(car2);
		kalle.getCars().add(car3);

		owners.put(olle.getSocialSecurityNo(), olle);
		owners.put(kalle.getSocialSecurityNo(), kalle);
		cars.put(car1.getRegNo(), car1);
		cars.put(car2.getRegNo(), car2);
		cars.put(car3.getRegNo(), car3);

	}

	public Car getCarByRegNo(String regNo) {
		return cars.get(regNo);
	}

	public List<Car> getCars() {
		return new ArrayList<Car>(cars.values());
	}

	public List<Owner> getOwners() {
		return new ArrayList<Owner>(owners.values());
	}

	public Owner getOwnerBySocialSecurityNo(String socialSecurityNo) {
		return owners.get(socialSecurityNo);
	}

	public void createOrUpdateCar(Car car) {
		cars.put(car.getRegNo(), car);
	}

}
