package se.plushogskolan.web.domain.cars.functions;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ShowDate {

	public static String todaysDate() {
		
		SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd HH:mm");		
		Date date = new Date();
		
		return simpleDate.format(date);
	}
}
