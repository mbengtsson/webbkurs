package se.plushogskolan.web.domain.cars;

public class Car {

	private String regNo;
	private int yearModel;
	private Brand brand;
	private double fuelConsumption;
	private Owner owner;

	public Car() {

	}

	public Car(String regNo, int yearModel, Brand brand, double fuelConsumption, Owner owner) {
		setRegNo(regNo);
		setYearModel(yearModel);
		setBrand(brand);
		setFuelConsumption(fuelConsumption);
		setOwner(owner);
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public int getYearModel() {
		return yearModel;
	}

	public void setYearModel(int yearModel) {
		this.yearModel = yearModel;
	}

	public double getFuelConsumption() {
		return fuelConsumption;
	}

	public void setFuelConsumption(double fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return "Car [regNo=" + regNo + ", yearModel=" + yearModel + ", brand=" + brand + ", fuelConsumption="
				+ fuelConsumption + ", owner=" + owner + "]";
	}

}
