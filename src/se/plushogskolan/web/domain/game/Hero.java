package se.plushogskolan.web.domain.game;

public enum Hero {

	TROLL("troll.png", 100, 100), PENGUIN("penguin.png", 100, 100), HERO("hero.png", 100, 100);

	private String image;
	private int width;
	private int heigh;

	private Hero(String image, int width, int height) {
		this.image = image;
		this.width = width;
		this.heigh = height;
	}

	public String getImageUrl() {
		return image;
	}

	public int getWidth() {
		return width;
	}

	public int getHeigh() {
		return heigh;
	}

}
