package se.plushogskolan.web.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.plushogskolan.web.domain.game.GameEngine;
import se.plushogskolan.web.domain.game.Hero;

public class Game extends HttpServlet {

	private GameEngine gameEngine = GameEngine.getInstance();
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.setAttribute("gameEngine", gameEngine);
		
		String direction = req.getParameter("direction");
		
		if (direction != null && direction.equals("up")) {
			gameEngine.moveTop();
		} else if (direction != null && direction.equals("down")) {
			gameEngine.moveBottom();
		} else if (direction != null && direction.equals("left")) {
			gameEngine.moveLeft();
		} else if (direction != null && direction.equals("right")) {
			gameEngine.moveRight();
		}
		
		String hero = req.getParameter("hero");
		
		if (hero != null && hero.equals("penguin")) {
			gameEngine.setHero(Hero.PENGUIN);
		} else if (hero != null && hero.equals("troll")) {
			gameEngine.setHero(Hero.TROLL);
		} else if (hero != null && hero.equals("hero")) {
			gameEngine.setHero(Hero.HERO);
		}
		
		if(req.getParameter("speed") != null) {
			gameEngine.setSpeed(Integer.parseInt(req.getParameter("speed")));
		}
		
		req.setAttribute("gameWidth", gameEngine.getGameWidth());
		req.setAttribute("gameHeight", gameEngine.getGameHeight());
		
		req.setAttribute("xPos", gameEngine.getxPos());
		req.setAttribute("yPos", gameEngine.getyPos());
		
		req.setAttribute("hero", gameEngine.getHero().getImageUrl());
		req.setAttribute("heroWidth", gameEngine.getHero().getWidth());
		req.setAttribute("heroHeight", gameEngine.getHero().getHeigh());
		
		req.setAttribute("speed", gameEngine.getSpeed());
		
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/game.jsp");
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	}

}
