package se.plushogskolan.web.servlets.sessions1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Sessions extends HttpServlet{
	
	List<String> languages;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		languages = new ArrayList<String>();
		languages.add("Swedish");
		languages.add("English");
		languages.add("German");
		languages.add("Spanish");
		
		
		req.setAttribute("languages", languages);
		
				
	RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/sessions1/setSessionValues.jsp");
	dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String name = req.getParameter("name");
		String language = req.getParameter("language");
		
		req.getSession().setAttribute("name", name);
				
		Cookie cookie = new Cookie("language", language);
		resp.addCookie(cookie);
		
		String url = resp.encodeURL(req.getContextPath() + "/servletexercises/jsp/sessions1/readSessionValues.jsp");
		
		resp.sendRedirect(url);

	}

}
