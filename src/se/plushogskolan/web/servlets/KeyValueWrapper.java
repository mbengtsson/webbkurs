package se.plushogskolan.web.servlets;

/**
 * A class that holds a key and a value. It is useful for printing information
 * from a Map. Instead of sending a whole Map to a jsp-page, we send a list of
 * wrappers, such as List<KeyValueWrapper>.
 * 
 * It is easier to iterate over a List than a Map.
 * 
 * The purpose is also to get familiar with the concept of a wrapper class.
 * Sometimes you have data in two different objects, but in the jsp page they
 * should be treated as a group of data belonging together. In those cases you
 * can create a wrapper class that holds the two other objects. This makes it
 * easy for us to iterate over a list of such wrappers, and for each iteration
 * we have easy access to the two objects. It would be much more difficult to
 * iterate over to lists and somehow keep them in synch.
 * 
 */
public class KeyValueWrapper {

	private String key;
	private String value;

	public KeyValueWrapper(String key, String value) {
		setKey(key);
		setValue(value);
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
