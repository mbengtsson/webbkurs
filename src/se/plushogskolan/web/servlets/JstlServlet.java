package se.plushogskolan.web.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import se.plushogskolan.web.domain.cars.Car;
import se.plushogskolan.web.domain.cars.CarService;
import se.plushogskolan.web.domain.cars.Owner;

public class JstlServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	Logger log = Logger.getLogger(JstlServlet.class);

	private CarService carService;

	public JstlServlet() {
		carService = CarService.getInstance();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// Add some texts with strange characters
		req.setAttribute("text1", "The product from \"Andersson & Svensson\" costs 100kr.");
		req.setAttribute("text2", "<b>This text is bold</b>");

		// Getting cars and owners
		List<Car> cars = carService.getCars();
		List<Owner> owners = carService.getOwners();
		req.setAttribute("cars", cars);
		req.setAttribute("owners", owners);

		// Check if we should show detailed information
		String showInfoParam = req.getParameter("showDetailedInformation");
		if (showInfoParam != null) {
			if ("true".equals(showInfoParam)) {
				req.setAttribute("detailedInformation",
						"This is some extra information with very important data, facts and figures.");
				;
			}
		}

		// Select a car when the form is submitted
		String carParam = req.getParameter("selectedCar");
		if (carParam != null) {
			Car selectedCar = carService.getCarByRegNo(carParam);
			req.setAttribute("selectedCar", selectedCar);
		}

		// A list of countries
		List<String> countries = new ArrayList<String>();
		countries.add("Sweden");
		countries.add("Denmark");
		countries.add("Norway");
		countries.add("Finland");
		countries.add("Island");
		req.setAttribute("countries", countries);

		req.getRequestDispatcher("/WEB-INF/jsp/jstl.jsp").forward(req, resp);
	}

}
