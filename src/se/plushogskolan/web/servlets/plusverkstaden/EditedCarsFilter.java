package se.plushogskolan.web.servlets.plusverkstaden;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditedCarsFilter implements Filter {

	private FilterConfig fc;
	private SessionUtil sessionUtil;

	@Override
	public void init(FilterConfig conf) throws ServletException {
		this.fc = conf;
		sessionUtil = SessionUtil.getInstance();

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {

		HttpServletRequest httpReq = (HttpServletRequest) req;
		HttpServletResponse httpResp = (HttpServletResponse) resp;

		Cookie editedCarsCookie = sessionUtil.getEditedCarsCookie(httpReq);

		List<String> editedCars = sessionUtil.getEditedCars(editedCarsCookie, httpReq);

		String lastEdited = (String) httpReq.getSession().getAttribute("lastEdited");

		if (lastEdited != null) {

			sessionUtil.addEditedCar(lastEdited, editedCars);
			sessionUtil.setEditedCarsAttribute(editedCars, httpReq);
			sessionUtil.setEditedCarsCookie(editedCarsCookie, editedCars, httpResp);
			
			httpReq.getSession().setAttribute("lastEdited", null);
		}
		
		sessionUtil.setCarsWithNamesAttribute(editedCars, httpReq);

		chain.doFilter(req, resp);

	}

	@Override
	public void destroy() {

	}
}