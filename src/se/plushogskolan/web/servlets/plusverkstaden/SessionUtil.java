package se.plushogskolan.web.servlets.plusverkstaden;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.plushogskolan.web.domain.cars.CarService;

public class SessionUtil {

	private static SessionUtil instance;

	private SessionUtil() {

	}

	public static SessionUtil getInstance() {
		if (instance == null) {
			instance = new SessionUtil();
		}

		return instance;
	}

	public void addEditedCar(String lastEdited, List<String> editedCars) {

		for (int i = 0; i < editedCars.size(); i++) {
			if (editedCars.get(i).equals(lastEdited)) {
				editedCars.remove(i);
			}
		}

		editedCars.add(0, lastEdited);

	}

	public Cookie getEditedCarsCookie(HttpServletRequest req) {

		Cookie[] cookies = req.getCookies();

		Cookie editedCarsCookie = null;

		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("editedCars")) {
					editedCarsCookie = cookie;
				}
			}
		}

		return editedCarsCookie;
	}

	public List<String> getEditedCars(Cookie cookie, HttpServletRequest req) {

		List<String> editedCars = new ArrayList<>();

		if (cookie != null && req.getSession().getAttribute("editedCars") == null) {

			editedCars = new ArrayList<String>(Arrays.asList(cookie.getValue().split(":")));

		} else if (req.getSession().getAttribute("editedCars") != null) {

			editedCars = (ArrayList<String>) req.getSession().getAttribute("editedCars");

		}

		return editedCars;
	}

	public void setEditedCarsCookie(Cookie cookie, List<String> editedCarsList, HttpServletResponse resp) {

		String editedCars = "";

		for (int i = 0; i < editedCarsList.size(); i++) {
			editedCars += editedCarsList.get(i);
			if (i != editedCarsList.size() - 1) {
				editedCars += ":";
			}
		}

		if (cookie != null) {
			cookie.setValue(editedCars);
		} else {
			cookie = new Cookie("editedCars", editedCars);
		}

		cookie.setMaxAge(31557600);
		resp.addCookie(cookie);

	}

	public void setEditedCarsAttribute(List<String> editedCarsList, HttpServletRequest req) {

		req.getSession().setAttribute("editedCars", editedCarsList);

	}

	public void setCarsWithNamesAttribute(List<String> editedCarsList, HttpServletRequest req) {

		List<String> carsWithNames = new ArrayList<>();

		for (int i = 0; i < editedCarsList.size(); i++) {
			
			try {
				String output = editedCarsList.get(i);
				output += " - " + CarService.getInstance().getCarByRegNo(editedCarsList.get(i)).getOwner().getFullName();
				carsWithNames.add(output);
			} catch (NullPointerException e) {
				carsWithNames.add("[" + editedCarsList.get(i) + " doesn't exist anymore]");
			}
		}

		req.setAttribute("carsWithNames", carsWithNames);
	}
}
