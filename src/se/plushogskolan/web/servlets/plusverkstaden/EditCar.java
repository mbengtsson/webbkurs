package se.plushogskolan.web.servlets.plusverkstaden;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.plushogskolan.web.domain.cars.Brand;
import se.plushogskolan.web.domain.cars.Car;
import se.plushogskolan.web.domain.cars.CarService;
import se.plushogskolan.web.domain.cars.Owner;

public class EditCar extends HttpServlet {

	private String regNo;
	private Car car;
	private List<Owner> owners;
	private Brand[] brands;
	private Boolean newCar;

	private boolean invalidRegNo;
	private boolean invalidYear;
	private boolean invalidBrand;
	private boolean invalidFuel;
	private boolean invalidOwner;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		newCar = false;

		regNo = req.getParameter("regNo");
		brands = Brand.values();
		owners = CarService.getInstance().getOwners();

		if (regNo != null && !regNo.equals("")) {

			car = CarService.getInstance().getCarByRegNo(regNo);

		} else {
			car = new Car();
			newCar = true;

		}

		req.setAttribute("car", car);
		req.setAttribute("brands", brands);
		req.setAttribute("owners", owners);
		req.setAttribute("newCar", newCar);

		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/plusverkstaden/EditCar.jsp");
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.setAttribute("car", car);
		req.setAttribute("brands", brands);
		req.setAttribute("owners", owners);
		req.setAttribute("newCar", newCar);

		validateForm(req, car);

		if (!invalidRegNo && !invalidYear && !invalidBrand && !invalidFuel && !invalidOwner) {

			CarService.getInstance().createOrUpdateCar(car);
			
			req.getSession().setAttribute("lastEdited", car.getRegNo());

			String url = resp.encodeURL(req.getContextPath() + "/plusverkstaden/listCars.servlet");
			resp.sendRedirect(url);
			
			
			
			return;

		} else {

			req.setAttribute("invalidReg", invalidRegNo);
			req.setAttribute("invalidYear", invalidYear);
			req.setAttribute("invalidBrand", invalidBrand);
			req.setAttribute("invalidFuel", invalidFuel);
			req.setAttribute("invalidOwner", invalidOwner);
		}

		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/plusverkstaden/EditCar.jsp");
		dispatcher.forward(req, resp);
	}

	/**
	 * Validates the input from the create/edit car-form and updates a
	 * car-object with the valid input
	 * 
	 * @param req
	 *            request from the form
	 * @param car
	 *            the car object
	 */
	private void validateForm(HttpServletRequest req, Car car) {

		invalidRegNo = true;
		invalidYear = true;
		invalidBrand = true;
		invalidFuel = true;
		invalidOwner = true;

		if (!req.getParameter("reg").equals("")) {
			car.setRegNo(req.getParameter("reg"));
			invalidRegNo = false;
		}

		try {
			int yearModel = Integer.parseInt(req.getParameter("year"));

			if (yearModel >= 1970 && yearModel <= 2013) {
				car.setYearModel(yearModel);
				invalidYear = false;
			}

		} catch (NumberFormatException e) {

		}

		if (!req.getParameter("brand").equals("empty")) {
			car.setBrand(Brand.valueOf(req.getParameter("brand")));
			invalidBrand = false;
		}

		try {
			double fuelConsumption = Double.parseDouble(req.getParameter("fuel"));
			if (fuelConsumption >= 0.1 && fuelConsumption <= 3) {
				car.setFuelConsumption(fuelConsumption);
				invalidFuel = false;
			} else if (fuelConsumption == 0.01) {
				throw new RuntimeException("Dummy exception from EditCarServlet");
			}

		} catch (NumberFormatException e) {

		}

		if (!req.getParameter("owner").equals("empty")) {
			car.setOwner(CarService.getInstance().getOwnerBySocialSecurityNo(req.getParameter("owner")));
			invalidOwner = false;
		}

	}
}
