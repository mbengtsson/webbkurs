package se.plushogskolan.web.servlets.plusverkstaden;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.plushogskolan.web.domain.cars.Car;
import se.plushogskolan.web.domain.cars.CarService;

public class ListCars extends HttpServlet {

	private CarService carService = CarService.getInstance();
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		List<Car> cars = carService.getCars();
		req.setAttribute("cars", cars);
		
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/plusverkstaden/ListCars.jsp");
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/plusverkstaden/ListCars.jsp");
		dispatcher.forward(req, resp);
	}

}
