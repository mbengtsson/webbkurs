package se.plushogskolan.web.servlets.sessions2;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class SelectCarServlet extends AbstractSessionServlet {

	private static final long serialVersionUID = 1L;
	private Logger log = Logger.getLogger(SelectCarServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		log.debug("SelectCarServlet.doGet");

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		log.debug("SelectCarServlet.doPost");
	}

}
