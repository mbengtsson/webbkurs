package se.plushogskolan.web.servlets.sessions2;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import se.plushogskolan.web.domain.cars.CarService;

public abstract class AbstractSessionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger log = Logger.getLogger(AbstractSessionServlet.class);
	protected CarService carService;

	public AbstractSessionServlet() {
		carService = CarService.getInstance();
	}

	/**
	 * Finds a value stored in a cookie.
	 * 
	 * @param cookieName
	 *            - the name of the cookie to look for
	 * @return returns the cookie value, or null if nothing is found
	 */
	protected String getValueFromCookie(HttpServletRequest req, String cookieName) {

		// A good method to implement in order to reuse code

		return null;
	}

	/**
	 * Add a cookie value to the response
	 */
	protected void addCookie(HttpServletResponse resp, String cookieName, String cookieValue) {

		// A good method to implement in order to reuse code
	}

	/**
	 * Clear the value of a cookie
	 */
	protected void clearCookie(HttpServletResponse resp, String cookieName) {

		// A good method to implement in order to reuse code

		// Hint: Set value to null and maxAge=0

	}

	/**
	 * Utility method to check if a String is either null or empty
	 */
	public boolean isEmpty(String value) {
		return value == null || value.length() == 0;
	}

	/**
	 * Utility method to check that a String is not null or empty
	 */
	public boolean isNotEmpty(String value) {
		return !isEmpty(value);
	}

}
