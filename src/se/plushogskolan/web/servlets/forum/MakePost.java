package se.plushogskolan.web.servlets.forum;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.plushogskolan.web.domain.forum.ForumService;
import se.plushogskolan.web.domain.forum.ForumThread;
import se.plushogskolan.web.domain.forum.Post;
import se.plushogskolan.web.domain.forum.User;

public class MakePost extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		User user = (User) req.getSession().getAttribute("user");

		if (user == null) {
			String url = resp.encodeURL(req.getContextPath() + "/forum/listThreads.servlet");
			resp.sendRedirect(url);

			return;
		}

		int threadId = Integer.parseInt(req.getParameter("threadId"));
		String postId = req.getParameter("postId");
		
		setAttributes(threadId, postId, req);

		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/forum/MakePost.jsp");
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		int threadId = Integer.parseInt(req.getParameter("threadId"));
		String postId = req.getParameter("postId");

		User user = (User) req.getSession().getAttribute("user");
		String title = req.getParameter("title");
		String message = req.getParameter("message");

		if (message.length() > 0 && title.length() > 0) {

			ForumThread thread = acquireThread(threadId, user, title);
			makePost(postId, thread, user, message, title);
			
			if (threadId < 0 && postId.equals("")) {
				ForumService.getInstance().addThread(thread);
			} else if (postId.equals("")) {
				ForumService.getInstance().removeThread(thread);
				ForumService.getInstance().addThread(thread);
			}
						
			String url = resp.encodeURL(req.getContextPath() + "/forum/readThread.servlet?thread=" + thread.getID());
			resp.sendRedirect(url);

			return;
		}

		setAttributes(threadId, postId, req);

		req.setAttribute("warning", "Both title and message is required");

		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/forum/MakePost.jsp");
		dispatcher.forward(req, resp);
	}

	private ForumThread acquireThread(int threadId, User user, String title) {

		ForumThread thread;

		if (threadId < 0) {
			thread = new ForumThread(user, title);
			threadId = thread.getID();
		} else {
			thread = ForumService.getInstance().getThreadByID(threadId);
		}

		return thread;
	}

	private void makePost(String postId, ForumThread thread, User user, String message, String title) {

		if (!postId.equals("")) {
			Post post = thread.getPosts().get(Integer.parseInt(postId));
			post.setTitle(title);

			SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String edited = "\r\n\r\n[edited]" + simpleDate.format(new Date()) + "[/edited]";

			post.setMessage(message + edited);
		} else {

			Post post = new Post(user, title, message);
			thread.addPost(post);
		}
	}
	
	private void setAttributes(int threadId, String postId, HttpServletRequest req) {
		if (threadId < 0) {
			req.setAttribute("newThread", true);
		} else {
			ForumThread thread = ForumService.getInstance().getThreadByID(threadId);
			req.setAttribute("thread", thread);
			if (postId != null && !postId.equals("")) {
				Post post = thread.getPosts().get(Integer.parseInt(postId));
				req.setAttribute("post", post);
				req.setAttribute("postId", postId);
			}
		}
	}
}