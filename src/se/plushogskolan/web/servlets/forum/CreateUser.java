package se.plushogskolan.web.servlets.forum;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.plushogskolan.web.domain.forum.ForumService;
import se.plushogskolan.web.domain.forum.User;

public class CreateUser extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/forum/CreateUser.jsp");
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String username = req.getParameter("username");
		String firstname = req.getParameter("firstname");
		String lastname = req.getParameter("lastname");
		String password = req.getParameter("password");
		Boolean inUse = false;

		List<User> users = ForumService.getInstance().getUsers();
		for (User user : users) {
			if (user.getAlias().equals(username)) {
				inUse = true;
			}
		}
		
		if (!inUse && !username.equals("") && !firstname.equals("") && !lastname.equals("") && !password.equals("")) {
			User user = new User(username, password, firstname, lastname);
			ForumService.getInstance().createUser(user);

			String url = resp.encodeURL(req.getContextPath() + "/forum/logIn.servlet");
			resp.sendRedirect(url);

			return;

		} else if (inUse) {
			req.setAttribute("warning", "The username is already taken");

		} else {
			req.setAttribute("warning", "You need to fill all fields");
		}

		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/forum/CreateUser.jsp");
		dispatcher.forward(req, resp);
	}

}
