package se.plushogskolan.web.servlets.forum;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.plushogskolan.web.domain.forum.ForumService;
import se.plushogskolan.web.domain.forum.ForumThread;
import se.plushogskolan.web.domain.forum.Post;
import se.plushogskolan.web.domain.forum.User;

public class ReadThread extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		int threadId = Integer.parseInt(req.getParameter("thread"));
		ForumThread thread = ForumService.getInstance().getThreadByID(threadId);
		List<Post> posts = thread.getPosts();

		req.setAttribute("thread", thread);
		req.setAttribute("posts", posts);

		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/forum/ReadThread.jsp");
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		int threadId = Integer.parseInt(req.getParameter("thread"));
		ForumThread thread = ForumService.getInstance().getThreadByID(threadId);
		List<Post> posts = thread.getPosts();

		int deleteId = Integer.parseInt(req.getParameter("deleteId"));
		thread.removePost(deleteId);

		if (thread.getPostCount() < 1) {
			ForumService.getInstance().removeThread(thread);

			String url = resp.encodeURL(req.getContextPath() + "/forum/listThreads.servlet");
			resp.sendRedirect(url);

			return;
		}

		req.setAttribute("thread", thread);
		req.setAttribute("posts", posts);

		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/forum/ReadThread.jsp");
		dispatcher.forward(req, resp);
	}

}
