package se.plushogskolan.web.servlets.forum;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.plushogskolan.web.domain.forum.ForumService;
import se.plushogskolan.web.domain.forum.ForumThread;
import se.plushogskolan.web.domain.forum.User;

public class ListThreads extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		User user = (User) req.getSession().getAttribute("user");
		List<ForumThread> threads = ForumService.getInstance().getThreads();

		req.setAttribute("user", user);
		req.setAttribute("threads", threads);

		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/forum/ListThreads.jsp");
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/forum/ListThreads.jsp");
		dispatcher.forward(req, resp);
	}

}
