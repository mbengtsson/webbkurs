package se.plushogskolan.web.servlets.forum;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.Session;

import se.plushogskolan.web.domain.forum.ForumService;
import se.plushogskolan.web.domain.forum.User;

public class LogIn extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		HttpSession session = req.getSession();
		if (session != null) {
			session.invalidate();
		}
		
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/forum/LogIn.jsp");
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String username = req.getParameter("username");
		String password = req.getParameter("password");

		User user = ForumService.getInstance().getUserByAlias(username);

		if (user != null && user.getPassword().equals(password)) {
			req.getSession().setAttribute("user", user);

			String url = resp.encodeURL(req.getContextPath() + "/forum/listThreads.servlet");
			resp.sendRedirect(url);

			return;

		} else {
			req.setAttribute("warning", "Username and password does not match");
		}

		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/forum/LogIn.jsp");
		dispatcher.forward(req, resp);
	}

}
