package se.plushogskolan.web.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.plushogskolan.web.domain.cars.Car;
import se.plushogskolan.web.domain.cars.CarService;

public class El extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		Car car = CarService.getInstance().getCarByRegNo("ABC-123");
		
		req.setAttribute("Car", car);
		
		req.setAttribute("message", "This is an info message in the request scope");
		req.getSession().setAttribute("message", "This is an info message in the session scope");
		req.getServletContext().setAttribute("message", "This is an info message in the application scope");
		
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/el.jsp");
		dispatcher.forward(req, resp);
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/el.jsp");
		dispatcher.forward(req, resp);
		
	}

}
