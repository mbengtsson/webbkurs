package se.plushogskolan.web.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RequestResponseServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
		String id = req.getParameter("id");
		
		if (id != null && id.equals("1")) {
			
			req.setAttribute("User", "Kalle");
			
		}else if (id != null && id.equals("2")) {
			
			req.setAttribute("User", "Stina");
			
		}
		
		resp.setHeader("Last-Modified", "Fri, 15 Nov 2013 12:45:26 +0000");
		
		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/requestResponse.jsp");
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/requestResponse.jsp");
		dispatcher.forward(req, resp);

	}
	
}
