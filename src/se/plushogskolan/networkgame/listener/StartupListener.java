package se.plushogskolan.networkgame.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import se.plushogskolan.networkgame.client.GameClient;
import se.plushogskolan.networkgame.server.GameServer;

/**
 * A listener that gets notified when the web application gets initialized or
 * destroyed. This is a good place to initialize the client and the server.
 * 
 */
public class StartupListener implements ServletContextListener {
	private static Logger log = Logger.getLogger(StartupListener.class);

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		log.debug("Servlet context destroyed. Shutting down server and client");
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		log.debug("Servlet context initialized. Starting up server and client");

		GameServer.getInstance();
		GameClient.getInstance();

	}

}
