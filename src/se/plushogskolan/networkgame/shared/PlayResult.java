package se.plushogskolan.networkgame.shared;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Class that represents the result of a play. If win > 0 the player was
 * successful, otherwise he lost.
 * 
 */
public class PlayResult {
	private int win;
	private int playerId;
	private int credit;

	public PlayResult() {

	}

	public PlayResult(int playerId, int win, int credit) {
		setPlayerId(playerId);
		setWin(win);
		setCredit(credit);
	}

	public static PlayResult parseJson(String json) throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, PlayResult.class);
		

	}

	public String toJson() throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		
		String json = mapper.writeValueAsString(this);
	
		return json;
	}

	public int getWin() {
		return win;
	}

	public void setWin(int win) {
		this.win = win;
	}

	public int getPlayerId() {
		return playerId;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	public int getCredit() {
		return credit;
	}

	public void setCredit(int amount) {
		this.credit = amount;
	}

	@Override
	public String toString() {
		return "PlayResult [win=" + win + ", playerId=" + playerId + ", credit=" + credit + "]";
	}

}
