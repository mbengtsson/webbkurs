package se.plushogskolan.networkgame.client;

import org.apache.log4j.Logger;

import se.plushogskolan.networkgame.client.domain.GameSession;
import se.plushogskolan.networkgame.shared.PlayResult;

/**
 * This class has a main method and can be started as a standalone java
 * application. It represents a client making requests. It can be used to test
 * the communication towards the game server.
 * 
 */
public class Main {
	private static Logger log = Logger.getLogger(Main.class);

	public static void main(String[] args) throws Exception {

		try {
			GameClient client = GameClient.getInstance();

			// Create a game
			int startingAmount = 1000;
			GameSession gameSession = client.createGame("Kalle", startingAmount);
			log.debug("Created game: " + gameSession);

			// Simulate some plays
			for (int i = 0; i < 10; i++) {
				PlayResult playResult = client.play(gameSession, 100);
				gameSession.setCredit(playResult.getCredit());
			}

			log.debug("Session after play: " + gameSession);
		} finally {
			log.debug("Exit game client");
			System.exit(0);
		}
	}

}
