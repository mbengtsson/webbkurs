package se.plushogskolan.networkgame.client;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import se.plushogskolan.networkgame.GameSettings;
import se.plushogskolan.networkgame.client.domain.GameSession;
import se.plushogskolan.networkgame.client.domain.PotChangedListener;
import se.plushogskolan.networkgame.client.network.HttpConnectionHelper;
import se.plushogskolan.networkgame.client.network.HttpParams;
import se.plushogskolan.networkgame.client.network.MulticastReceiverThread;
import se.plushogskolan.networkgame.client.network.SocketHelper;
import se.plushogskolan.networkgame.shared.PlayResult;

/**
 * The main game logic for the client.
 * 
 */
public class GameClient {
	private static Logger log = Logger.getLogger(GameClient.class);

	private static GameClient instance;
	private Set<PotChangedListener> potListeners = new HashSet<PotChangedListener>();

	public static GameClient getInstance() {
		if (instance == null) {
			try {
				instance = new GameClient();
			} catch (Exception e) {
				throw new RuntimeException("Game client could not be started: " + e.getMessage());
			}
		}
		return instance;
	}

	private GameClient() throws Exception {

		log.debug("Starting new game client");

		// Start a thread that listens to multicast messages
		new MulticastReceiverThread(GameSettings.MULTICAST_ADDRESS, GameSettings.MULTICAST_PORT).start();

	}

	public GameSession createGame(String name, int amount) throws Exception {

		// Send a request to the server to create a new player
		String response = SocketHelper.sendSocketRequest("Create new player: Amount=" + amount);
		log.debug("Response from server: " + response);

		// Parse playerId and amount
		Pattern pattern = Pattern.compile("(\\d*):(\\d*)");
		Matcher matcher = pattern.matcher(response);
		if (matcher.matches()) {
			int playerId = Integer.parseInt(matcher.group(1));
			int amountFromServer = Integer.parseInt(matcher.group(2));

			// Create GameSession
			GameSession gameSession = new GameSession(name, playerId, amountFromServer);
			log.debug("Created game session: " + gameSession);
			return gameSession;

		} else {
			throw new RuntimeException("Exception creating game. Socket response did not contain player id and amount.");
		}
	}

	public PlayResult play(GameSession session, int amount) {

		try {

			// Send a play message to the server using HTTP. The server will
			// respond with data in json format. Use HttpConnectionHelper to
			// send the message. When the json response arrives, transform the
			// json data to a PlayMessage object. Remove the dummy logic below.

			HttpParams httpParams = new HttpParams();
			httpParams.setParam("playerId", Integer.toString(session.getPlayerId()));
			httpParams.setParam("amount", Integer.toString(amount));

			URL url = new URL(GameSettings.PLAY_URL);
			String response = HttpConnectionHelper.performPOST(url, httpParams, "text/json");

			log.debug("json Http-response: " + response);

			PlayResult playResult = PlayResult.parseJson(response);

			return playResult;

			// Start dummy logic
			// double random = Math.random();
			// if (random > 0.5) {
			// log.debug("Random number: " + random + ". This means you win.");
			// return new PlayResult(1, 100, 1100);
			// } else {
			// log.debug("Random number: " + random +
			// ". This means you loose.");
			// return new PlayResult(1, 0, 900);
			// }
			// End dummy logic

		} catch (Exception e) {
			throw new RuntimeException("Exception performing play: " + e.getMessage(), e);
		}
	}

	public void handleUpdatedPot(int pot) {
		log.debug("handle update pot: " + pot + ". Notifying " + potListeners.size() + " listeners. ");

		for (PotChangedListener listener : potListeners) {
			listener.onPotChanged(pot);
		}

	}

	public void addPotListener(PotChangedListener listener) {
		potListeners.add(listener);
	}

}
