package se.plushogskolan.networkgame.client.network;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * A custom class to handle parameters that should be sent using HTTP. This
 * class takes care of encoding the parameters and joining them together using
 * proper HTTP syntax.
 * 
 */
public class HttpParams {
	private Map<String, String> params = new HashMap<String, String>();

	/**
	 * Add a new parameter with a given name and value
	 */
	public void setParam(String paramName, String paramValue) {
		params.put(paramName, paramValue);
	}

	/**
	 * Convert the parameters to a single String.
	 */
	public String getHttpParameterString() throws Exception {

		// Convert to a list of key=value strings
		List<String> paramList = new ArrayList<String>();
		for (String paramName : params.keySet()) {

			// Encode parameter values so they can be transmitted correctly.
			// Otherwise they might have illegal characters that will ruin the
			// transfer.
			String paramValue = URLEncoder.encode(params.get(paramName), "UTF-8");
			paramList.add(paramName + "=" + paramValue);
		}

		// Join all parameters into one string and use & as separator
		return StringUtils.join(paramList, "&");
	}
}
