package se.plushogskolan.networkgame.client.network;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.apache.log4j.Logger;

import se.plushogskolan.networkgame.GameSettings;
import se.plushogskolan.networkgame.client.GameClient;

/**
 * A helper class to send messages and receive messages using sockets.
 * 
 */
public class SocketHelper {

	private static Logger log = Logger.getLogger(GameClient.class);

	public static String sendSocketRequest(String requestMsg) {

		try {
			log.debug("sendSocketRequest: '" + requestMsg + "'");

			// Create socket. Use the constants in GameSettings.java for IP and
			// port.

			Socket socket = new Socket(GameSettings.CLIENT_ADDRESS, GameSettings.SOCKET_PORT);

			// Send the requestMsg parameter to the socket

			PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
			out.println(requestMsg);

			// Read the response from socket

			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String response = in.readLine();

			log.debug("Response from server: " + response);

			// Returning dummy data since server is not activated.
			// The format is: playerId:amount
			// When this method is fully implemented, return the response from
			// the server instead.

			return response;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Error in sendSocketRequest: " + e.getMessage());
		}
	}
}
