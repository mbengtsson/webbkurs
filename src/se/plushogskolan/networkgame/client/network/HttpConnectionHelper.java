package se.plushogskolan.networkgame.client.network;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.log4j.Logger;

/**
 * A helper class to send http requests using the java API
 * 
 */
public class HttpConnectionHelper {

	private static Logger log = Logger.getLogger(HttpConnectionHelper.class);

	/**
	 * Performs a POST to a given url.
	 * 
	 * @param url
	 *            - The url to post to
	 * @param params
	 *            - The request parameters to write to the request body
	 * @param acceptHeader
	 *            - The type of content we accept as response. Formatted as a
	 *            valid mime type.
	 * @return The response content
	 */
	public static String performPOST(URL url, HttpParams params, String acceptHeader) throws Exception {

		String parameterString = params.getHttpParameterString();
		log.debug("Sending POST to " + url + " with parameters " + parameterString);

		HttpURLConnection connection = null;
		try {
			// Create connection
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Accept", acceptHeader);
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestProperty("Content-Length", Integer.toString(parameterString.getBytes().length));

			// Configure the connection
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			// Send request. Write the parameters to the request body. Compare
			// this to GET requests where we can simply append the parameters to
			// the URL

			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(parameterString);
			wr.flush();
			wr.close();

			// Read the response data
			String response = readInputStream(connection.getInputStream());
			log.debug("Response from POST: " + url);
			log.debug(response);

			return response;

		} catch (Exception e) {
			throw new RuntimeException("Exception in performPOST: " + e.getMessage(), e);

		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}

	}

	/**
	 * A utility method to read an InputStream
	 */
	private static String readInputStream(InputStream in) throws Exception {

		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		StringBuilder response = new StringBuilder();
		String inputLine;
		while ((inputLine = reader.readLine()) != null) {
			response.append(inputLine);
		}
		reader.close();
		return response.toString();
	}
}
