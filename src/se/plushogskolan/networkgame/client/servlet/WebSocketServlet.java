package se.plushogskolan.networkgame.client.servlet;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.StreamInbound;
import org.apache.catalina.websocket.WsOutbound;
import org.apache.log4j.Logger;

import se.plushogskolan.networkgame.client.GameClient;
import se.plushogskolan.networkgame.client.domain.PotChangedListener;

/**
 * This is a special kind of servlet designed to handle web sockets connections
 * using Tomcat 7.
 * 
 */
@SuppressWarnings("deprecation")
public class WebSocketServlet extends org.apache.catalina.websocket.WebSocketServlet implements PotChangedListener {

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(WebSocketServlet.class);

	// A collection of all the current web socket connections.
	protected Set<SocketInbound> connections = new HashSet<SocketInbound>();

	@Override
	public void init() throws ServletException {
		super.init();
		log.debug("WebSocketServlet starting up...");

		// Register this servlet as a pot listener. When the pot changes this
		// servlet will be called with the new pot amount. We then use
		// websockets to update all the active browser windows.
		GameClient.getInstance().addPotListener(this);

	}

	/**
	 * A new web socket connection is created.
	 */
	protected StreamInbound createWebSocketInbound(String arg0, HttpServletRequest req) {

		return new SocketInbound();

	}

	/**
	 * Method used to write a text message to all the connected web sockets.
	 * 
	 */
	private void broadcast(String text) {
		for (SocketInbound si : connections) {

			try {
				si.getWsOutbound().writeTextMessage(CharBuffer.wrap(text));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onPotChanged(int newPot) {
		log.debug("received new pot: " + newPot);

		String json = String.format("{\"pot\":\"%d\"}", newPot);

		log.debug("Sending json: " + json);
		broadcast(json);

	}

	/**
	 * This class is created for every web socket connections.
	 * 
	 * If there are 2 users connected, there will be two instances of
	 * SocketInbound. If user 1 submits a web socket message, that message will
	 * be received in SocketInbound no 1.
	 * 
	 * The WebSocketServlet keeps a list of all connections, and can therefore
	 * broadcast a message to all web pages that are connected.
	 * 
	 */
	public class SocketInbound extends MessageInbound {

		@Override
		protected void onBinaryMessage(ByteBuffer buffer) throws IOException {
			log.debug("OnBinaryMessage");

		}

		@Override
		protected void onTextMessage(CharBuffer chars) throws IOException {
			log.debug("OnTextMessage: " + chars);

			String text = chars.toString();

			// Do something with incoming message
		}

		@Override
		protected void onClose(int status) {
			super.onClose(status);
			log.debug("Socket is closed");
			connections.remove(this);
		}

		@Override
		protected void onOpen(WsOutbound outbound) {
			super.onOpen(outbound);
			log.debug("Socket is opened");
			connections.add(this);
		}
	}

}
