package se.plushogskolan.networkgame.client.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.plushogskolan.networkgame.client.GameClient;
import se.plushogskolan.networkgame.client.domain.GameSession;

public class RegisterServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.getRequestDispatcher("/WEB-INF/jsp/networkgame/client/register.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {

			String name = req.getParameter("name");
			int amount = Integer.parseInt(req.getParameter("amount"));

			// Create a new game session
			GameSession gameSession = GameClient.getInstance().createGame(name, amount);

			// Set GameSession in the user's session
			req.getSession().setAttribute("gameSession", gameSession);

			resp.sendRedirect(req.getContextPath() + "/networkgame/client/play.servlet");
		} catch (Exception e) {
			throw new ServletException("Unable to create game: " + e.getMessage(), e);
		}

	}

}
