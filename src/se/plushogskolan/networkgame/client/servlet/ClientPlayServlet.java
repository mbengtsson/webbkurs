package se.plushogskolan.networkgame.client.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import se.plushogskolan.networkgame.client.GameClient;
import se.plushogskolan.networkgame.client.domain.GameSession;
import se.plushogskolan.networkgame.shared.PlayResult;

/**
 * The servlet for handling the client playing.
 * 
 */
public class ClientPlayServlet extends HttpServlet {

	private static Logger log = Logger.getLogger(ClientPlayServlet.class);
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.getRequestDispatcher("/WEB-INF/jsp/networkgame/client/play.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		log.debug("Entering doPost()");

		try {
			// Read parameters
			int amount = Integer.parseInt(req.getParameter("amount"));

			log.debug("New amount: " + amount);

			GameSession gameSession = (GameSession) req.getSession().getAttribute("gameSession");
			gameSession.setAmountLastBet(amount);

			// Delegate logic to the GameClient
			PlayResult playResult = GameClient.getInstance().play(gameSession, amount);
			gameSession.setCredit(playResult.getCredit());

			log.debug("Recieving play-result from server: " + playResult);
			// Convert to JSON.
			String json = playResult.toJson();

			// TODO: Print json to the output stream
			resp.setContentType("application/json");

			PrintWriter writer = resp.getWriter();
			writer.print(json);
			writer.flush();
			writer.close();

		} catch (Exception e) {
			throw new RuntimeException("Exception in ClientPlayServlet: " + e.getMessage(), e);
		}

	}
}
