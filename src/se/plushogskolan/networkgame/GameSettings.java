package se.plushogskolan.networkgame;

/**
 * Some useful constants
 */
public class GameSettings {

	// You might have to change this
	public static final String CONTEXTPATH = "webb";

	public static final int SOCKET_PORT = 9090;
	public static final int UDP_PORT = 9091;
	public static final int MULTICAST_PORT = 9092;
	public static final String CLIENT_ADDRESS = "localhost";
	public static final String MULTICAST_ADDRESS = "230.0.0.1";
	public static final String HTTP_PORT = "8080";
	public static final String PLAY_URL = "http://localhost:8080/" + CONTEXTPATH + "/networkgame/play.servlet";
}
