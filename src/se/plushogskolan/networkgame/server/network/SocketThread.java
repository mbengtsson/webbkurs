package se.plushogskolan.networkgame.server.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import se.plushogskolan.networkgame.server.GameServer;
import se.plushogskolan.networkgame.server.domain.Player;

/**
 * Thread that listens to incoming messages from a client socket.
 * 
 */
public class SocketThread extends Thread {

	private static Logger log = Logger.getLogger(SocketThread.class);
	private ServerSocket serverSocket;

	public SocketThread(int port) throws Exception {

		super("SocketThread");
		log.debug("Socket thread started on port " + port);
		serverSocket = new ServerSocket(port);
	}

	public void run() {
		try {

			while (true) {

				// Wait for incoming client calls. This is done using the
				// accept() method on the serverSocket.

				Socket clientSocket = serverSocket.accept();

				try {

					// When we get here a client has made a connection. This is
					// a good time to prepare input- and output streams.

					InputStream inputStream = clientSocket.getInputStream();
					InputStreamReader reader = new InputStreamReader(inputStream);
					BufferedReader bufferedReader = new BufferedReader(reader);

					PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);

					// Read the incoming message from the socket
					// For now we are using a hardcoded message

					String input = bufferedReader.readLine();
					log.debug("Socket request: " + input);

					// Handle business logic
					String response = null;
					if (input.startsWith("Create new player")) {
						response = handleNewPlayer(input);
					}

					// Send the response back to client

					out.println(response);
					log.debug("Sending response: " + response);

				} finally {
					log.debug("Closing server socket");
					// Close the client socket
				}

			}

		} catch (Exception e) {
			throw new RuntimeException("Exception when creating server socket: " + e.getMessage(), e);
		} finally {
			try {
				serverSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * A client socket is requesting the server to create a new player. The
	 * client is also sending the amount the new player wants to deposit.
	 */
	private String handleNewPlayer(String input) {

		log.debug("handleNewPlayer: " + input);

		// Parse amount using regular expression
		Pattern pattern = Pattern.compile(".*Amount=(\\d+).*");
		Matcher matcher = pattern.matcher(input);
		if (matcher.matches()) {
			int amount = Integer.parseInt(matcher.group(1));

			Player player = GameServer.getInstance().createNewPlayer(amount);
			return player.getId() + ":" + amount;
		} else {
			throw new RuntimeException("Could not parse amount from socket input: " + input);
		}
	}
}
