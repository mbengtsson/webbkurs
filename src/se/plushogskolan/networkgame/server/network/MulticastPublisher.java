package se.plushogskolan.networkgame.server.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import org.apache.log4j.Logger;

/**
 * A class that can send messages to a multicast address, thus notifying many
 * clients at the same time.
 * 
 */
public class MulticastPublisher {
	private static Logger log = Logger.getLogger(MulticastPublisher.class);
	protected DatagramSocket socket = null;
	private String ip;
	private int port;

	public MulticastPublisher(String ip, int port) throws IOException {
		log.debug("MulticastPublisher: ip=" + ip + ", port=" + port);
		this.ip = ip;
		this.port = port;
		socket = new DatagramSocket();
	}

	public void sendMessage(String msg) {

		byte[] data = new byte[128];

		try {
			InetAddress address = InetAddress.getByName(ip);

			data = msg.getBytes();
			DatagramPacket packet = new DatagramPacket(data, data.length, address, port);

			log.debug("Sending message: " + msg + " to: " + ip + ":" + port);
			socket.send(packet);

			// Create a DatagramPacket and send it using the DatagramSocket.
			// The IP and port are provided in the constructor.

		} catch (Exception e) {
			throw new RuntimeException("Exception sending multicast message: " + e.getMessage(), e);
		}
	}

}
