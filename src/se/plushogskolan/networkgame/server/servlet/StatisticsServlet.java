package se.plushogskolan.networkgame.server.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import se.plushogskolan.networkgame.server.GameServer;

/**
 * Servlet that displays server statistics.
 * 
 */
public class StatisticsServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(StatisticsServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		log.debug("doGet");

		GameServer gameServer = GameServer.getInstance();
		int noOfPlayers = gameServer.getNoOfPlayers();
		int pot = gameServer.getPot().getAmount();
		int totalAmount = gameServer.getTotalPlayerCredits();

		req.setAttribute("noOfPlayers", noOfPlayers);
		req.setAttribute("pot", pot);
		req.setAttribute("totalAmount", totalAmount);

		String acceptHeader = req.getHeader("Accept");
		if ("text/json".equals(acceptHeader)) {
			req.getRequestDispatcher("/WEB-INF/jsp/networkgame/server/statisticsJson.jsp").forward(req, resp);
		} else {
			req.getRequestDispatcher("/WEB-INF/jsp/networkgame/server/statistics.jsp").forward(req, resp);
		}

	}

}
