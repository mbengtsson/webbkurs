package se.plushogskolan.networkgame.server.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import se.plushogskolan.networkgame.server.GameServer;
import se.plushogskolan.networkgame.shared.PlayResult;

/**
 * Servlet that accepts POST calls to execute a play for a user.
 * 
 * The game server will handle the play, and return a PlayResult that indicates
 * it the player won or lost.
 * 
 * This servlet returns data in the json format.
 * 
 */
public class PlayServlet extends HttpServlet {
	private static Logger log = Logger.getLogger(PlayServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		log.info("Calling PlayServlet with GET. This is not recommended. But ok for debugging purposes.");

		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			log.debug("Entering PlayServlet");

			// TODO: Parse the "playerId" and "amount" parameters from the
			// request

			int playerId = Integer.parseInt(req.getParameter("playerId"));
			int amount = Integer.parseInt(req.getParameter("amount"));
			log.debug("Play: player=" + playerId + ", amount=" + amount);

			// Call game server
			PlayResult playResult = GameServer.getInstance().play(playerId, amount);

			// Convert to json
			String json = playResult.toJson();

			// Print json to the output stream
			PrintWriter writer = new PrintWriter(resp.getOutputStream());
			writer.println(json);
			writer.close();

		} catch (Exception e) {
			throw new RuntimeException("Exception in PlayServlet: " + e.getMessage(), e);
		}

	}

}
