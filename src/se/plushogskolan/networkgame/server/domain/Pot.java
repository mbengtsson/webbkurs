package se.plushogskolan.networkgame.server.domain;

/**
 * The current pot that is available for all players.
 * 
 */
public class Pot {
	private int amount;

	public Pot(int initialPot) {
		setAmount(initialPot);
	}

	public void addToPot(int amount) {
		setAmount(getAmount() + amount);
	}

	public void withdrawFromPot(int amount) {
		setAmount(getAmount() - amount);
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
}
