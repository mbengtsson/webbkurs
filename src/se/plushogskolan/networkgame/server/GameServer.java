package se.plushogskolan.networkgame.server;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import se.plushogskolan.networkgame.GameSettings;
import se.plushogskolan.networkgame.server.domain.Player;
import se.plushogskolan.networkgame.server.domain.Pot;
import se.plushogskolan.networkgame.server.network.MulticastPublisher;
import se.plushogskolan.networkgame.server.network.SocketThread;
import se.plushogskolan.networkgame.shared.PlayResult;

/**
 * The main server logic of the game.
 * 
 */
public class GameServer {
	private static Logger log = Logger.getLogger(GameServer.class);
	private static GameServer instance;

	private Pot pot = new Pot(1000);
	private MulticastPublisher multicastPublisher;
	private Map<Integer, Player> players = new HashMap<Integer, Player>();

	public static synchronized GameServer getInstance() {
		if (instance == null) {
			try {
				instance = new GameServer();
			} catch (Exception e) {
				throw new RuntimeException("GameServer could not be started: " + e.getMessage(), e);
			}
		}
		return instance;
	}

	private GameServer() {
		try {
			log.debug("Game server starting up...");

			// Start a new thread to listen to sockets
			new SocketThread(GameSettings.SOCKET_PORT).start();

			// Create a multicast publisher
			multicastPublisher = new MulticastPublisher(GameSettings.MULTICAST_ADDRESS, GameSettings.MULTICAST_PORT);

			log.debug("Game server started successfully");
		} catch (Exception e) {
			throw new RuntimeException("Could not start GameServer: " + e.getMessage(), e);
		}
	}

	public Player createNewPlayer(int amount) {
		Player player = Player.createPlayerWithGeneratedId();
		player.setAmount(amount);
		players.put(player.getId(), player);
		log.debug("Created a new player: " + player);
		return player;
	}

	public int getNoOfPlayers() {
		return players.size();
	}

	public Pot getPot() {
		return pot;
	}

	public int getTotalPlayerCredits() {
		int totalCredits = 0;
		for (Player p : players.values()) {
			totalCredits += p.getAmount();
		}
		return totalCredits;
	}

	public PlayResult play(int playerId, int playerBet) {

		Player player = getPlayer(playerId);
		int win = calculateWinOrLose(playerBet);

		// Update pot and user amount depending on play outcome
		if (win > 0) {
			getPot().withdrawFromPot(win);
			player.addWin(win);
		} else {
			player.withDrawAmount(playerBet);
			getPot().addToPot(playerBet);
		}

		// Broadcast new pot
		multicastPublisher.sendMessage("pot:" + getPot().getAmount());

		return new PlayResult(player.getId(), win, player.getAmount());
	}

	/**
	 * The logic for deciding if the player is winning or losing.
	 */
	private int calculateWinOrLose(int playerBet) {

		double random = Math.random();
		log.debug("Random nr in play: " + random);
		int win = 0;
		if (random > 0.95) {
			win = playerBet * 10;
		} else if (random > 0.8) {
			win = playerBet * 5;
		} else if (random > 0.7) {
			win = playerBet;
		}
		log.debug("Win: " + win);
		return win;
	}

	public Player getPlayer(int playerId) {
		Player player = players.get(playerId);
		if (player != null) {
			return player;
		} else {
			throw new RuntimeException("No player found for id " + playerId);
		}
	}
}
